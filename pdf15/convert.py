import os
import shutil
import subprocess
from pathlib import Path

from pypdf import PdfReader


def convert(path, backup=False):
    for pdf_file in get_pdf_files(path):
        if get_pdf_version(pdf_file) != '1.5':
            backup_file = create_backup_file(pdf_file)
            convert_file(backup_file, pdf_file)

            if not backup:
                os.remove(backup_file)


def get_pdf_files(path):
    directory = Path(path)
    return sorted(directory.glob('*.pdf'))


def get_pdf_version(pdf_file):
    reader = PdfReader(pdf_file)
    return reader.pdf_header.split('-')[1]


def create_backup_file(file):
    backup_file = file.with_name(file.stem + '.bak' + file.suffix)
    shutil.copy(file, backup_file)
    return backup_file


def convert_file(src, dest):
    command = [
        'gs',
        '-dCompatibilityLevel=1.5',
        '-dNOPAUSE',
        '-dBATCH',
        '-sDEVICE=pdfwrite',
        f'-sOutputFile={dest}',
        src
    ]

    subprocess.run(command)
