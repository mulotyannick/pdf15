import argparse

from . import __version__, convert


def cli():
    parser = argparse.ArgumentParser(prog='pdf15',
                                     description='Converts a PDF documents to PDF 1.5.')

    parser.add_argument('path', nargs='?', default='.', help='The path to a directory containing PDF documents')
    parser.add_argument('--backup', action='store_true', help="Keep backup files")
    parser.add_argument('-v', '--version', action='version', version=__version__)
    args = parser.parse_args()

    convert(args.path, backup=args.backup)


if __name__ == "__main__":
    cli()
