# Commande pdf15

Commande CLI permettant la conversion de fichiers PDF vers la version 1.5.
L'objectif est de ne plus avoir d'alerte lorsque j'insère un document PDF 1.7 dans un document LaTeX.

## Installation

Les applications `poetry` et `pipx` doivent être installées sur votre environnement.

```bash
poetry build
pipx install dist/pdf15-1.0.0.tar.gz
```